﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
namespace NGN
{
    public class NGNEntityStateController : NGNEntity
    {
        [System.Serializable]
        public class EntityStateTransition
        {
            [SerializeField] protected bool anyState;
            public bool IsAnyState { get { return anyState; } }
            [SerializeField] protected EntityStateData prevState;
            public EntityStateData PrevState { get { return nextState; } }
            [SerializeField] protected EntityStateTriggerData trigger;
            public EntityStateTriggerData Trigger { get { return trigger; } }
            [SerializeField] protected EntityStateData nextState;
            public EntityStateData NextState { get { return nextState; } }
        }

        [SerializeField] protected EntityStateData startState;
        [SerializeField] protected EntityStateTransition[] stateTransitions;

        protected EntityStateData currentState;
        public EntityStateData CurrentState { get { return currentState; } }
        protected EntityStateTriggerData.OnTriggerDelegate triggerDelegateCache;
        protected override void Awake()
        {
            base.Awake();
            OnBeginStates();
        }
        
        protected virtual void OnBeginStates()
        {
            currentState = startState;
            InitializeCurrentState();
            ResetTransitions();
        }

        protected virtual void InitializeCurrentState()
        {
            currentState.Initialize(this);
        }

        private void ResetTransitions()
        {
            for (int i = 0; i < stateTransitions.Length; i++)
            {
                var trans = stateTransitions[i];
                if (trans != null)
                {
                    var next = trans.NextState;
                    if (trans.Trigger)
                    {
                        triggerDelegateCache = SetCurrentState;
                        trans.Trigger.OnTrigger += triggerDelegateCache;
                        trans.Trigger.OnInitialize(next);
                    }
                        
                } 
            }
        }

        public virtual void SetCurrentState(EntityStateTriggerData _trigger, EntityStateData _stateData)
        {
            _trigger.OnTrigger -= triggerDelegateCache;
            currentState.StopActions(this);
            currentState = _stateData;
            InitializeCurrentState();
            ResetTransitions();
        }

    }
}


