Thanks for testing the 3D Engine!
======================================
SSH: bitbucket.org:toomasio/3dengine.git
HTTPS: https://toomasio@bitbucket.org/toomasio/3dengine.git
Trello/Bug Reporting: https://trello.com/b/oBv9RlSH
======================================
Before updating:
- Close Unity
- Pull latest changes in a separate project used ONLY for pulling my updates...name it 3DEngine
- DELETE YourUnityProject/Assets/3DEngine in file explorer. DO NOT MERGE FOLDERS!
- Copy the updated 3DEngine/Assets/3DEngine to YourUnityProject/Assets. DO NOT MERGE FOLDERS!
- Open your project using Unity and your project should now be updated!