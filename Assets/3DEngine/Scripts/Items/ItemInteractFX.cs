﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInteractFX : ItemAimable
{
    public new ItemInteractFXData Data { get { return (ItemInteractFXData)data; } }
}
